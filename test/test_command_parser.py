from durations import Duration

from jmenu.backend.arguments.argument import Argument
from jmenu.backend.arguments.argument_types import (
    TextArgumentType,
    DurationArgumentType,
)
from jmenu.backend.command import Command


def test_parse_single_text():
    command = Command("ex1", [Argument(name="arg1", value="", type=TextArgumentType())])
    command = command.parse("test")
    assert command.args[0].value == "test"


def test_parse_single_text_current_arg():
    command = Command("ex1", [Argument(name="arg1", value="", type=TextArgumentType())])
    command = command.parse("test")
    assert command.current_arg_index == 0
    command = command.parse("test ")
    assert command.current_arg_index == 0
    command = command.parse("test hello")
    assert command.current_arg_index == 0


def test_parse_duration_text_text():
    command = Command(
        "ex1",
        [
            Argument(name="arg1", value="", type=DurationArgumentType()),
            Argument(name="arg2", value="", type=TextArgumentType()),
            Argument(name="arg3", value="", type=TextArgumentType()),
        ],
    )
    command = command.parse("1h test1 test2")
    assert command.args[0].value.seconds == Duration("1h").seconds
    assert command.args[1].value == "test1 test2"
    assert command.args[2].value == ""
    assert command.current_arg_index == 1
