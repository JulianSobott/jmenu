from jmenu.backend.extensions.hedgedoc import extension as hd_extension
from jmenu.backend.extensions import interpreter
from jmenu.backend.extensions.model import IntField, Field


def test_it():
    interpreter.config_handle_auth(hd_extension.config.authentication)
    count_command = hd_extension.commands[0].commands[2]

    interpreter.run_data_pipeline(count_command.action.result)


def test_extension_to_yaml():
    import yaml

    print(yaml.dump(hd_extension.dict(), sort_keys=False))


def test_jin():
    f = IntField(value="1")
    handle = Field.get_handler(f.type)
    print(handle(value=f.value))
