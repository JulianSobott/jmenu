from dataclasses import dataclass
from datetime import datetime

from jmenu.backend.recommender.recommender import Recommender, RecommendationRepository, Context, Result, Reward


def test_recommendation():
    # GIVEN
    recommender = Recommender()
    query = "dra"
    context = Context(hour=0, weekday=0, dt=datetime.now(), query=query)
    result = Result(text="drawio")
    reward = Reward(query_length=len(query), time_till_select_s=5)

    # WHEN
    recommender.learn(context, result, reward)
    recommendations = recommender.recommend(context)

    # THEN
    assert recommendations[0].result.text == "drawio"
