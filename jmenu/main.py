import logging

logging.basicConfig(level=logging.DEBUG)

from jmenu.backend.authentication import auth_manager
from jmenu.backend.backend import Backend
from jmenu.events import QueryChangedEvent, EventBus
from jmenu.gui.pyside_gui import PysideGui

if __name__ == "__main__":
    event_bus = EventBus()
    backend = Backend()
    app = PysideGui()
    app.mainloop()
