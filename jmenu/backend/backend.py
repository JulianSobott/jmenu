from jmenu.backend.extensions.fwfin_dsl import extension as fwfin_extension
from jmenu.backend.extensions.hedgedoc import extension as hedgedoc_extension
from jmenu.backend.extensions.app_finder import extension as app_finder_extension
from jmenu.backend.extensions.calculator import extension as calculator_extension
from jmenu.backend.extensions.plotter import extension as plotter_extension
from jmenu.backend.extensions.dummy import extension as dummy_extension
from jmenu.backend.extensions.model import Extension, Command
from jmenu.backend.matcher.argument import ArgumentMatcher
from jmenu.backend.matcher.base import MatcherImpl, MatcherRepository
from jmenu.events import (
    QueryChangedEvent,
    event_bus,
    ResultsChangedEvent,
    ExtensionRegisteredEvent,
    SetArgumentMatchingModeEvent,
)


class Backend:
    def __init__(self):
        self.matchers: list[MatcherImpl] = []
        extensions = extension_repository.get_extensions()
        for extension in extensions:
            for matcher in extension.matcher:
                matcher_impl = MatcherRepository.get_item_for_model(matcher)(matcher)
                matcher_impl.on_load()
                self.matchers.append(matcher_impl)
        event_bus.subscribe(QueryChangedEvent, self.on_query_changed)
        event_bus.subscribe(
            SetArgumentMatchingModeEvent, self.set_argument_matching_mode
        )
        self.is_arg_matching_mode = False
        self.arg_matcher: ArgumentMatcher | None = None

    def on_query_changed(self, event: QueryChangedEvent) -> None:
        query = event.query
        if query.query == "":
            event_bus.publish(ResultsChangedEvent([]))
            return
        results = []
        if self.is_arg_matching_mode:
            if self.arg_matcher:
                results = self.arg_matcher.match(query)
        else:
            for matcher in self.matchers:
                matcher_results = matcher.match(query)
                if matcher_results:
                    results.extend(matcher_results)
            if self.is_arg_matching_mode:
                return  # do not publish results if in argument matching mode
        results.sort(key=lambda x: x.score, reverse=True)
        event_bus.publish(ResultsChangedEvent(results))

    def set_argument_matching_mode(self, event: SetArgumentMatchingModeEvent) -> None:
        self.is_arg_matching_mode = event.is_argument_matching_mode
        self.arg_matcher = event.matcher
        event_bus.publish(QueryChangedEvent(event.query))


class ExtensionRepository:
    def __init__(self):
        # TODO: load extensions from config
        self.extensions: dict[str, Extension] = {
            hedgedoc_extension.id: hedgedoc_extension,
            fwfin_extension.id: fwfin_extension,
            app_finder_extension.id: app_finder_extension,
            calculator_extension.id: calculator_extension,
            plotter_extension.id: plotter_extension,
            dummy_extension.id: dummy_extension,
        }
        for extension in self.extensions.values():
            event_bus.publish(ExtensionRegisteredEvent(extension))

    def get_extension(self, extension_id: str) -> Extension:
        return self.extensions[extension_id]

    def get_extensions(self) -> list[Extension]:
        return list(self.extensions.values())


extension_repository = ExtensionRepository()
