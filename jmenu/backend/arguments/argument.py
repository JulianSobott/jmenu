import random
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Callable


class ArgumentType(ABC):
    @abstractmethod
    def matches(self, value: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def display_name(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def parse(self, value: str) -> object:
        raise NotImplementedError


@dataclass
class RuntimeData:
    start: int
    end: int
    color: str = None


@dataclass
class Argument:
    name: str
    value: object
    type: ArgumentType
    default: str = ""
    required: bool = False
    autocomplete: bool = False
    get_autocomplete_values: Callable[["Command", str], list["Result"]] = None
    runtime_data: "RuntimeData" = None
    # autocomplete_values: list[Result] = Non
