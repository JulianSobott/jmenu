from jmenu.backend.arguments.argument import ArgumentType

from durations import Duration


class TextArgumentType(ArgumentType):
    def matches(self, value: str) -> bool:
        return True

    def display_name(self) -> str:
        return "Text"

    def parse(self, value: str) -> object:
        return value


class NumberArgumentType(ArgumentType):
    def matches(self, value: str) -> bool:
        return value.isnumeric()

    def display_name(self) -> str:
        return "Number"

    def parse(self, value: str) -> object:
        if "." in value:
            return float(value)
        else:
            return int(value)


class IntegerArgumentType(ArgumentType):
    def matches(self, value: str) -> bool:
        return value.isnumeric()

    def display_name(self) -> str:
        return "Integer"

    def parse(self, value: str) -> object:
        return int(value)


class BooleanArgumentType(ArgumentType):
    def __init__(self):
        self.true_values = ["true", "1", "yes", "y"]
        self.false_values = ["false", "0", "no", "n"]

    def matches(self, value: str) -> bool:
        value_lower = value.lower()
        return value_lower in self.true_values or value_lower in self.false_values

    def display_name(self) -> str:
        return "Boolean"

    def parse(self, value: str) -> object:
        value_lower = value.lower()
        return value_lower in self.true_values


class FloatArgumentType(ArgumentType):
    def matches(self, value: str) -> bool:
        return value.replace(".", "", 1).isnumeric()

    def display_name(self) -> str:
        return "Float"

    def parse(self, value: str) -> object:
        return float(value)


class DurationArgumentType(ArgumentType):
    def matches(self, value: str) -> bool:
        if (
            " " in value
        ):  # TODO: this only accepts durations of format 10h, 0.25h etc. but simplifies the code
            return False
        try:
            parsed = Duration(value)
            if parsed.seconds <= 0:
                return False
            return True
        except Exception:
            return False

    def display_name(self) -> str:
        return "Duration"

    def parse(self, value: str) -> object:
        return Duration(value)


class SelectableArgumentType(ArgumentType):
    def __init__(self, item_exists):
        self.item_exists = item_exists

    def matches(self, value: str) -> bool:
        return self.item_exists(value)

    def display_name(self) -> str:
        return "Selectable"

    def parse(self, value: str) -> object:
        return value
