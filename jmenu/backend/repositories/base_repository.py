import logging
from typing import TypeVar, Generic

M = TypeVar("M")  # model
L = TypeVar("L")  # Logic


class Repository(Generic[M, L]):
    items: dict[type[M], L] = {}
    logger = logging.getLogger(__name__)

    @classmethod
    def item_for(cls, model: type[M]):
        def decorator(item):
            cls.logger.info(f"Registering item {item} for model {model}")
            cls.items[model] = item
            return item

        return decorator

    @classmethod
    def get_item_for_model(cls, model: M) -> L:
        return cls.get_item_for(type(model))

    @classmethod
    def get_item_for(cls, model: type[M]) -> L:
        return cls.items.get(model, cls.not_found(model))

    @staticmethod
    def not_found(model_type: type[M]) -> L:
        def not_found() -> L:
            raise NotImplementedError(f"Item for model {model_type} not found")

        return not_found
