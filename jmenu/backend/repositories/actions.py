import webbrowser
from abc import abstractmethod

import jinja2

from jmenu.backend.extensions.model import Action, OpenUrlAction
from jmenu.backend.repositories.base_repository import Repository
from jmenu.backend.repositories.form_widgets import FormItem
from jmenu.events import ActionFinishedEvent, event_bus


class ActionImpl:
    def __init__(self, model: Action, form_fields: list[FormItem]):
        self.model = model
        self.form_fields = form_fields

    @abstractmethod
    def execute(self):
        raise NotImplementedError


class ActionRepository(Repository[Action, ActionImpl]):
    pass


@ActionRepository.item_for(OpenUrlAction)
class OpenUrlActionImpl(ActionImpl):
    def execute(self):
        arguments = {}
        for field in self.form_fields:
            arguments[field.model.name] = field.get_value()
        print(arguments)
        url = jinja2.Template(self.model.url).render(
            **{"this": {"arguments": arguments}}
        )
        print(url)
        webbrowser.open(url)
        event_bus.publish(ActionFinishedEvent())
