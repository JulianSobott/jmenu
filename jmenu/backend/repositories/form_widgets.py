from abc import abstractmethod

from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget

from jmenu.backend.extensions.model import TextArgument, DateArgument, Argument
from jmenu.backend.repositories.base_repository import Repository


class FormItem:
    def __init__(self, model: Argument):
        self.model = model

    @abstractmethod
    def render(self) -> QWidget:
        raise NotImplementedError

    @abstractmethod
    def get_value(self):
        raise NotImplementedError


class FormWidgetRepository(Repository[Argument, FormItem]):
    @staticmethod
    def not_found(model_type: type[FormItem]):
        class NotFound(FormItem):
            def __init__(self, model: Argument):
                super().__init__(model)

            def get_value(self):
                return None

            def render(self) -> QWidget:
                widget = QtWidgets.QWidget()
                widget.layout = QtWidgets.QHBoxLayout()
                widget.setLayout(widget.layout)
                widget.layout.addWidget(
                    QtWidgets.QLabel(
                        f"No widget for {model_type.__name__} for argument {self.model.name}"
                    )
                )
                return widget

        return NotFound


@FormWidgetRepository.item_for(TextArgument)
class TextArgumentWidget(FormItem):
    def __init__(self, model: TextArgument):
        super().__init__(model)
        self.model = model
        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QHBoxLayout()
        self.widget.setLayout(self.layout)
        self.text_input = QtWidgets.QLineEdit()
        self.layout.addWidget(self.text_input)
        self.text = ""
        self.text_input.textChanged.connect(self.on_text_changed)

    def on_text_changed(self, text):
        self.text = text

    def render(self) -> QWidget:
        return self.widget

    def get_value(self):
        return self.text


@FormWidgetRepository.item_for(DateArgument)
class DateArgumentWidget(FormItem):
    def __init__(self, model: DateArgument):
        super().__init__(model)
        self.model = model
        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QHBoxLayout()
        self.widget.setLayout(self.layout)
        self.date_input = QtWidgets.QCalendarWidget()
        self.layout.addWidget(self.date_input)

    def render(self) -> QWidget:
        return self.widget

    def get_value(self):
        return self.date_input.selectedDate().getDate()
