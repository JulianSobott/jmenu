from dataclasses import dataclass
from datetime import datetime

from thefuzz import fuzz


@dataclass
class Result:
    text: str


@dataclass
class ScoredResult:
    result: Result
    score: int

    def __lt__(self, other: "ScoredResult") -> bool:
        return self.score < other.score

    def __repr__(self) -> str:
        return f"ScoredResult({self.result}, {self.score})"


@dataclass
class Context:
    query: str
    hour: int
    weekday: int
    dt: datetime


@dataclass
class Reward:
    query_length: int
    time_till_select_s: int

    def score(self) -> int:
        return self.query_length * self.time_till_select_s


@dataclass
class Usage:
    context: Context
    reward: Reward


@dataclass
class Recommendation:
    result: Result
    usages: list[Usage]


class Recommender:

    def __init__(self):
        self.recommendations = RecommendationRepository()

    def recommend(self, context: Context, num_items: int = 5) -> list[ScoredResult]:
        scored_results = []
        for recommendation in self.recommendations.get_recommendations().values():
            score = self.score_recommendation(context, recommendation)
            scored_result = ScoredResult(result=recommendation.result, score=score)
            scored_results.append(scored_result)
        return [r for r in sorted(scored_results, reverse=True)][:num_items]

    def learn(self, context: Context, result: Result, reward: Reward) -> None:
        self.recommendations.add_recommendation(context, result, reward)

    def score_recommendation(self, context: Context, recommendation: Recommendation) -> int:
        score = 0
        score += self.score_query(context, recommendation)
        score += self.score_time(context, recommendation)
        score += self.score_usage(context, recommendation)
        return score

    def score_query(self, context: Context, recommendation: Recommendation) -> int:
        return fuzz.ratio(context.query, recommendation.result.text)

    def score_time(self, context: Context, recommendation: Recommendation) -> int:
        return 0

    def score_usage(self, context: Context, recommendation: Recommendation) -> int:
        score = 0
        for usage in recommendation.usages:
            score += usage.reward.score()
        return score


class RecommendationRepository:

    def __init__(self):
        self.recommendations = {}

    def get_recommendations(self) -> dict[str, Recommendation]:
        return self.recommendations

    def add_recommendation(self, context: Context, result: Result, reward: Reward) -> None:
        if result.text not in self.recommendations:
            self.recommendations[result.text] = Recommendation(result=result, usages=[])
        self.recommendations[result.text].usages.append(Usage(context=context, reward=reward))
