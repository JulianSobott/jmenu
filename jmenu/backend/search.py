import os
from abc import abstractmethod

import xdg.DesktopEntry
import xdg.BaseDirectory

from jmenu.backend.result import Result, AppResult
from jmenu.events import Query, ResultsChangedEvent, event_bus, AppSelectedEvent

from thefuzz import fuzz


class ScoredResult:

    def __init__(self, result: Result, score: int):
        self.result = result
        self.score = score

    def __lt__(self, other: "ScoredResult") -> bool:
        return self.score < other.score

    def __repr__(self) -> str:
        return f"ScoredResult({self.result}, {self.score})"


class ResultContainer:

    def __init__(self):
        self.max_results = 10
        self.remove_threshold = 0
        self.results: list[ScoredResult] = []

    def clear(self) -> None:
        self.results.clear()

    def add_results(self, results: list[ScoredResult]) -> None:
        for result in results:
            self.add_result(result)

    def add_result(self, result: ScoredResult) -> None:
        if result.score < self.remove_threshold:
            return
        self.results.append(result)

    def sorted_results(self) -> list[ScoredResult]:
        return [r for r in sorted(self.results, reverse=True)][:self.max_results]


result_container = ResultContainer()


class AbstractSearch:

    def __init__(self):
        self.result_container = result_container

    @abstractmethod
    def search(self, keyword: str) -> list[ScoredResult]:
        raise NotImplementedError

    def on_change(self, keyword: str) -> None:
        results = self.search(keyword)
        self.result_container.add_results(results)

    @abstractmethod
    def get_selected(self) -> Result | None:
        raise NotImplementedError

    @abstractmethod
    def publish_selected(self):
        raise NotImplementedError


class AppSearch(AbstractSearch):

    def __init__(self):
        super().__init__()
        self.apps: dict[str, xdg.DesktopEntry.DesktopEntry] = {}
        self.load_apps()
        self.selected_app = None

    def load_apps(self):
        for directory in xdg.BaseDirectory.load_data_paths("applications"):
            for app in os.listdir(directory):
                if not app.endswith(".desktop"):
                    continue
                app = xdg.DesktopEntry.DesktopEntry(os.path.join(directory, app))
                self.apps[app.getName()] = app

    def search(self, keyword: str) -> list[ScoredResult]:
        results = []
        self.selected_app = None
        for app in self.apps.values():
            if app.getHidden():
                continue
            part = keyword.lower()
            full = app.getName().lower()
            score = fuzz.ratio(part, full)
            if full.startswith(part):
                score = 100
                self.selected_app = app
            if score > 90:
                # add actions as additional results
                actions = app.getActions()
                for action in actions:
                    results.append(ScoredResult(AppResult(app, action), score))
            results.append(ScoredResult(AppResult(app), score))
        return results

    def get_selected(self) -> Result | None:
        if self.selected_app:
            return AppResult(self.selected_app)
        return None

    def publish_selected(self):
        event_bus.publish(AppSelectedEvent(self.selected_app))

    def get_app(self, app_id: str) -> xdg.DesktopEntry.DesktopEntry:
        return self.apps[app_id]


app_search = AppSearch()
