import json
import logging
from collections import defaultdict
from pathlib import Path
import keyring
from PySide6 import QtCore


class Configuration:
    def __init__(self):
        self.settings = QtCore.QSettings("jmenu", "jmenu")
        self.logger = logging.getLogger(__name__)

    def get_value_for(self, extension, key):
        return self.settings.value(f"{extension.id}/{key}", None)

    def set_value_for(self, extension, key, value):
        self.settings.setValue(f"{extension.id}/{key}", value)

    def get_secret_for(self, extension, key):
        return keyring.get_password(extension.id, key)

    def set_secret_for(self, extension, key, value):
        keyring.set_password(extension.id, key, value)


config = Configuration()
