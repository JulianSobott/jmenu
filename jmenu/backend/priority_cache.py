import json
import logging
import os
from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
from typing import Callable

from pydantic import BaseModel

CacheObject = BaseModel


class Cache:

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.path = "cache.json"
        self.cache = defaultdict(dict)
        # self.load()
        # self.deserializers = {}

    def register_deserializer(self, type_: str, deserializer: Callable[[dict], CacheObject]):
        self.deserializers[type_] = deserializer

    def push(self, cache_type: str, key: str, obj: CacheObject):
        if key not in self.cache[cache_type]:
            self.cache[cache_type][key] = CacheEntry(
                type=cache_type, name=key, payload=obj, added=datetime.now(), last_accessed=datetime.now(), accessed_count=0
            )
        else:
            self.cache[cache_type][key].last_accessed = datetime.now()
            self.cache[cache_type][key].accessed_count += 1
        self.persist()

    def persist(self):
        cache_file = CacheFile(entries=self.cache)
        with open(self.path, "w") as f:
            f.write(cache_file.model_dump_json())

    def load(self):
        if not os.path.exists(self.path):
            return
        try:
            with open(self.path, "r") as f:
                data = json.load(f)
                # hook into pydantic to deserialize the payload
                for cache_type in data["entries"].keys():
                    for key in data["entries"][cache_type].keys():
                        entry = data["entries"][cache_type][key]
                        entry["payload"] = self.deserializers[cache_type](entry["payload"])
                cache_file = CacheFile.parse_obj(data)
                self.cache = cache_file.entries
        except json.decoder.JSONDecodeError as e:
            self.logger.error(f"Failed to load cache: {e}")

    def get_top_entries(self, num: int = 3) -> list["CacheEntry"]:
        all_entries = []
        for type_ in self.cache.keys():
            all_entries += list(self.cache[type_].values())
        entries = sorted(all_entries, key=lambda x: x.accessed_count, reverse=True)
        return entries[:num]


class CacheEntry(BaseModel):
    type: str
    name: str
    payload: CacheObject
    added: datetime
    last_accessed: datetime
    accessed_count: int


class CacheFile(BaseModel):
    entries: dict[str, dict[str, CacheEntry]]


cache = Cache()
