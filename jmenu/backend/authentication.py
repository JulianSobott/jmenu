import logging
from collections import defaultdict

from jmenu.backend.extensions.model import Extension
from jmenu.events import event_bus, ExtensionRegisteredEvent


class AuthenticationManager:
    def __init__(self):
        self.extensions_with_authentication: list[Extension] = []

        event_bus.subscribe(ExtensionRegisteredEvent, self.on_extension_registered)
        self.logger = logging.getLogger(__name__)

    def on_extension_registered(self, event: ExtensionRegisteredEvent):
        extension = event.extension
        self.logger.info(f"Registered extension {extension.name}")
        if extension.config.authentication:
            self.logger.info(f"Extension {extension.name} requires authentication")
            self.extensions_with_authentication.append(extension)

    def get_extensions_with_authentication(self):
        return self.extensions_with_authentication


auth_manager = AuthenticationManager()
