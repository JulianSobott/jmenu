import logging
import re
import webbrowser
from abc import abstractmethod
from dataclasses import dataclass
from typing import Callable

import pyperclip
import xdg.DesktopEntry
import xdg.IconTheme
from PySide6 import QtGui
from PySide6 import QtWidgets
from PySide6.QtCore import Qt, QKeyCombination

from jmenu.backend.extensions.model import Command, Action
from jmenu.backend.repositories.form_widgets import FormWidgetRepository, FormItem
from jmenu.backend.repositories.actions import ActionRepository
from jmenu.events import event_bus, ActionFinishedEvent, SetQueryEvent, Query


class Result:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def get_actions(self) -> list["Event"]:
        """returns a list of actions that can be performed on this result"""
        return []

    @abstractmethod
    def render(self, layout: QtWidgets.QLayout) -> QtWidgets.QWidget:
        raise NotImplementedError


@dataclass
class Context:
    query: Query


@dataclass
class Event:
    name: str
    keys: list[QKeyCombination]
    description: str
    do: Callable[[Result, Context], None]


def copy_to_clipboard(text: str, exit_after: bool = True):
    def execute(*_):
        pyperclip.copy(text)
        if exit_after:
            event_bus.publish(ActionFinishedEvent())

    return execute


def set_query(query: str):
    def execute(*_):
        event_bus.publish(SetQueryEvent(Query(query)))

    return execute


def execute_action(action: Action, form_fields: list[FormItem]):
    def execute(*_):
        ActionRepository.get_item_for_model(action)(action, form_fields).execute()
        event_bus.publish(ActionFinishedEvent())

    return execute


class CommandResult(Result):
    def __init__(self, command: Command, parent_commands: list[Command] = None):
        super().__init__()
        self.command = command
        self.parent_commands = parent_commands or []

    def get_actions(self) -> list[Event]:
        return [
            Event(
                name="fill in",
                keys=[QKeyCombination(Qt.Key_Tab)],
                description="Fill in command",
                do=set_query(self.on_fill_in()),
            ),
            Event(
                name="substitute alias",
                keys=[QKeyCombination(Qt.Key_Space)],
                description="Substitute alias with command",
                do=self.check_substitute_alias,
            ),
        ]

    def on_fill_in(self):
        if self.parent_commands:
            return (
                " ".join([command.command for command in self.parent_commands])
                + " "
                + self.command.command
                + " "
            )
        else:
            return self.command.command + " "

    def render(self, layout: QtWidgets.QLayout):
        container = QtWidgets.QWidget()
        container.setStyleSheet("QWidget { background-color: #333333; }")
        lbl_name = QtWidgets.QLabel(self.command.name)
        lbl_name.setStyleSheet("QLabel { color : white; font-size: 20px; }")

        lbl_parent_commands = QtWidgets.QLabel(
            " -> ".join([command.command for command in self.parent_commands])
        )
        lbl_parent_commands.setStyleSheet("QLabel { color : #aaaaaa; }")

        lbl_command = QtWidgets.QLabel(self.command.command)
        lbl_command.setStyleSheet("QLabel { color : #aaaaaa; }")

        container_layout = QtWidgets.QVBoxLayout()
        container_layout.setAlignment(Qt.AlignLeft)
        container_layout.addWidget(lbl_name)
        command_layout = QtWidgets.QHBoxLayout()
        command_layout.addWidget(lbl_parent_commands)
        command_layout.addWidget(lbl_command)
        container_layout.addLayout(command_layout)
        container.setLayout(container_layout)

        layout.addWidget(container)
        return container

    def check_substitute_alias(self, result: Result, ctx: Context):
        # check if the last command is an alias and substitute it with the command
        if self.command.aliases:
            last_command = ctx.query.query.split(" ")[-1]
            if last_command in self.command.aliases:
                new_query_parts = ctx.query.query.split(" ")[0:-1] + [
                    self.command.command
                ]
                new_query = " ".join(new_query_parts)
                set_query(new_query)(result, ctx)


class AppResult(Result):
    """native app with .desktop file"""

    def __init__(self, app: xdg.DesktopEntry.DesktopEntry, action: str = None):
        super().__init__()
        self.app = app
        self.app_id = app.getName()
        self.action = action

    def get_actions(self) -> list[Event]:
        return [
            Event(
                name="launch",
                keys=[QKeyCombination(Qt.Key_Return)],
                description="Launch app",
                do=lambda res, ctx: self.on_selected(),
            ),
            Event(
                name="copy",
                keys=[QKeyCombination(Qt.CTRL | Qt.Key_C)],
                description="Copy app name to clipboard",
                do=copy_to_clipboard(self.app.getName()),
            ),
        ]

    def render(self, layout: QtWidgets.QLayout):
        # left icon, top text, bottom description
        container = QtWidgets.QWidget()
        container.setStyleSheet("QWidget { background-color: #333333; }")
        icon = QtWidgets.QLabel()
        icon_name = self.app.getIcon()
        pixmap = None
        if icon_name:
            icon_path = xdg.IconTheme.getIconPath(icon_name, 64)
            if icon_path:
                pixmap = QtGui.QPixmap(icon_path)
        if not pixmap:
            pixmap = QtGui.QPixmap("resources/default.png")
        if pixmap:
            icon.setPixmap(pixmap.scaled(64, 64))
        icon.setStyleSheet("QLabel { color : white; }")
        name = QtWidgets.QLabel(self.app.getName())
        name.setStyleSheet("QLabel { color : white; font-size: 20px; }")
        description = QtWidgets.QLabel(self.app.getComment())
        description.setStyleSheet("QLabel { color : #aaaaaa; }")

        # exec command
        exec_line = self.app.getExec()
        exec_label = QtWidgets.QLabel(exec_line)
        exec_label.setStyleSheet("QLabel { color : white; }")

        # action
        if self.action:
            action_label = QtWidgets.QLabel(f"--> {self.action}")
            action_label.setStyleSheet("QLabel { color : white; }")

        container_layout = QtWidgets.QHBoxLayout()
        container_layout.setAlignment(Qt.AlignLeft)
        container_layout.addWidget(icon)
        content_layout = QtWidgets.QVBoxLayout()

        title_layout = QtWidgets.QHBoxLayout()
        title_layout.setAlignment(Qt.AlignLeft)
        title_layout.addWidget(name)
        if self.action:
            title_layout.addWidget(action_label)

        content_layout.addLayout(title_layout)
        content_layout.addWidget(description)
        content_layout.addWidget(exec_label)
        container_layout.addLayout(content_layout)
        container.setLayout(container_layout)
        layout.addWidget(container)
        return container

    def on_selected(self):
        self.launch_app()
        event_bus.publish(ActionFinishedEvent())

    def launch_app(self):
        # parse app properties and launch accordingly
        import subprocess
        import shlex

        if self.action:
            needed_group = f"Desktop Action {self.action}"
            if needed_group not in self.app.groups():
                self.logger.warning(
                    f"app {self.app.getName()} does not have group {needed_group}. Available groups: {self.app.groups()}"
                )
            else:
                self.app.defaultGroup = needed_group

        exec_line = self.app.getExec()
        if self.app.getTerminal():
            exec_line = f"x-terminal-emulator -e {exec_line()}"
        exec_line = re.sub(r"%[uUfFdDnNickvm]", "", exec_line).strip()
        self.logger.debug(f"executing {exec_line}")
        subprocess.Popen(shlex.split(exec_line))


class FormResult(Result):
    def __init__(self, command: Command, parent_commands: list[Command] = None):
        super().__init__()
        self.command = command
        self.parent_commands = parent_commands or []
        self.form_fields = []
        for argument in self.command.arguments:
            self.form_fields.append(
                FormWidgetRepository.get_item_for_model(argument)(argument)
            )

    def get_actions(self) -> list[Event]:
        return [
            Event(
                name="post",
                keys=[QKeyCombination(Qt.Key_Return)],
                description="Post form",
                do=execute_action(self.command.action, self.form_fields),
            )
        ]

    def render(self, layout: QtWidgets.QLayout) -> QtWidgets.QWidget:
        # create a form to fill all arguments
        container = QtWidgets.QWidget()
        container.setStyleSheet("QWidget { background-color: white; }")
        lbl_name = QtWidgets.QLabel(self.command.name)
        lbl_name.setStyleSheet("QLabel { color : white; font-size: 20px; }")

        lbl_parent_commands = QtWidgets.QLabel(
            " -> ".join([command.command for command in self.parent_commands])
        )
        lbl_parent_commands.setStyleSheet("QLabel { color : #aaaaaa; }")

        container_header_layout = QtWidgets.QHBoxLayout()
        container_header_layout.setAlignment(Qt.AlignLeft)
        container_header_layout.addWidget(lbl_parent_commands)
        container_header_layout.addWidget(lbl_name)

        form_layout = QtWidgets.QFormLayout()
        for item in self.form_fields:
            form_layout.addRow(item.model.name, item.render())

        container_layout = QtWidgets.QVBoxLayout()
        container_layout.setAlignment(Qt.AlignLeft)
        container_layout.addLayout(container_header_layout)
        container_layout.addLayout(form_layout)
        container.setLayout(container_layout)

        layout.addWidget(container)
        return container


class AutocompleteResult(Result):
    def __init__(self, text: str):
        super().__init__()
        self.text = text

    def render(self, layout: QtWidgets.QLayout) -> QtWidgets.QWidget:
        container = QtWidgets.QWidget()
        container.setStyleSheet("QWidget { background-color: #333333; }")
        lbl_name = QtWidgets.QLabel(self.text)
        lbl_name.setStyleSheet("QLabel { color : white; font-size: 20px; }")
        container_layout = QtWidgets.QVBoxLayout()
        container_layout.setAlignment(Qt.AlignLeft)
        container_layout.addWidget(lbl_name)
        container.setLayout(container_layout)
        layout.addWidget(container)
        return container
