from thefuzz import fuzz

from jmenu.backend.extensions.model import Command
from jmenu.backend.matcher.argument import ArgumentMatcher
from jmenu.backend.matcher.base import MatcherImpl, MatcherRepository
from jmenu.backend.result import FormResult, CommandResult
from jmenu.backend.search import ScoredResult
from jmenu.events import Query, SetArgumentMatchingModeEvent, event_bus


@MatcherRepository.item_for(Command)
class CommandMatcher(MatcherImpl[Command]):
    depth_multiplier = 0.9
    max_depth = 3

    def match(self, query: Query) -> list[ScoredResult]:
        """
        syntax for commands:
        command (subcommand)* (argument)*

        difficulties:
        - string arguments with spaces
            - quotes (single, double) -> start with this. special syntax is possible
            - delimiter -> adds quotes
            - keybindings -> adds quotes
        - can a command with subcommands have arguments?

        idea argument matching:
        - change global state to "argument mode" via event
        - this creates a new Matcher (all other matchers are disabled)
        """
        if query.query == "":
            return []
        if query.query[0] in ["?", "="]:  # TODO: reserved characters
            return []
        results = self._recursive_match(query, self.model, [], depth_multiplier=1.0)
        return results

    def _recursive_match(
        self,
        query: Query,
        command: Command,
        parents: list[Command],
        depth_multiplier: float,
    ) -> list[ScoredResult]:
        if parents:
            parent_string = " ".join([parent.command for parent in parents]) + " "
        else:
            parent_string = ""
        results = []
        new_depth_multiplier = depth_multiplier
        if query.query.startswith(f"{parent_string}{command.command} "):
            # spaces are important. command is already matched
            if command.arguments:
                event_bus.publish(
                    SetArgumentMatchingModeEvent(
                        True, ArgumentMatcher(command, parents), query
                    )
                )
                return []
        else:
            child_targets = [command.command] + command.aliases
            targets = [f"{parent_string}{target}" for target in child_targets]
            score = max([fuzz.ratio(query.keyword, target) for target in targets])
            res = ScoredResult(
                CommandResult(command=command, parent_commands=parents), score
            )
            results.append(res)
            new_depth_multiplier = depth_multiplier * self.depth_multiplier
        if len(parents) >= self.max_depth:
            return results
        for subcommand in command.commands:
            results += self._recursive_match(
                query, subcommand, parents + [command], new_depth_multiplier
            )
        return results
