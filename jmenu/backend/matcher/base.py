from typing import TypeVar, Generic

from jmenu.backend.extensions.model import Matcher
from jmenu.backend.repositories.base_repository import Repository
from jmenu.backend.search import ScoredResult
from jmenu.events import Query

M = TypeVar("M", bound=Matcher)


class MatcherImpl(Generic[M]):
    def __init__(self, model: M):
        self.model = model

    def on_load(self):
        pass

    def match(self, query: Query) -> list[ScoredResult]:
        raise NotImplementedError


class MatcherRepository(Repository[Matcher, MatcherImpl]):
    pass
