import os

import xdg
from thefuzz import fuzz

from jmenu.backend.extensions.app_finder import AppFinderMatcher
from jmenu.backend.matcher.base import MatcherImpl, MatcherRepository
from jmenu.backend.result import AppResult
from jmenu.backend.search import ScoredResult
from jmenu.events import Query


@MatcherRepository.item_for(AppFinderMatcher)
class AppFinderMatcherImpl(MatcherImpl[AppFinderMatcher]):
    def __init__(self, model: AppFinderMatcher):
        super().__init__(model)
        self.apps: dict[str, xdg.DesktopEntry.DesktopEntry] = {}

    def on_load(self) -> None:
        # docs: https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
        # attributes: https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s06.html
        for directory in xdg.BaseDirectory.load_data_paths("applications"):
            for app in os.listdir(directory):
                if not app.endswith(".desktop"):
                    continue
                app = xdg.DesktopEntry.DesktopEntry(os.path.join(directory, app))
                if app.getNoDisplay():
                    continue
                self.apps[app.getName()] = app

    def match(self, query: Query):
        if len(query.query) == 0:
            return
        results = []
        for app in self.apps.values():
            if app.getHidden():
                continue
            part = query.query.lower()
            full = app.getName().lower()
            score = fuzz.ratio(part, full)
            if full.startswith(part):
                score = 100
            if score > 90:
                # add actions as additional results
                actions = app.getActions()
                for action in actions:
                    results.append(ScoredResult(AppResult(app, action), score))
            results.append(ScoredResult(AppResult(app), score))
        return results
