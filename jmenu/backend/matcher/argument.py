from jmenu.backend.arguments.argument import Argument
from jmenu.backend.extensions.model import Command
from jmenu.backend.matcher.base import MatcherImpl
from jmenu.backend.result import AutocompleteResult
from jmenu.backend.search import ScoredResult
from jmenu.events import Query, SetArgumentMatchingModeEvent, event_bus


class ArgumentMatcher(MatcherImpl[Command]):
    def __init__(self, model: Command, parents: list[Command]):
        super().__init__(model)
        self.parents = parents or []

    def match(self, query: Query) -> list[ScoredResult]:
        """
        1. checks if command is still valid. if not deactivate argument matching mode

        tasks:
        - show possible arguments
        - show autocomplete for arguments
        - surround arguments with quotes if necessary
        """
        prefix = " ".join([parent.command for parent in self.parents]) + " "
        if not query.query.startswith(prefix + self.model.command):
            event_bus.publish(SetArgumentMatchingModeEvent(False, None, query))
            return []
        arg_query = query.query[len(f"{prefix}{self.model.command} ") :]
        if arg_query == "":
            return []
        return [ScoredResult(AutocompleteResult("Hello world"), 100)]
