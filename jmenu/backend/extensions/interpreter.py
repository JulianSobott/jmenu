import pyjq
import requests
import jinja2

from jmenu.backend.extensions.model import (
    DataPipeline,
    DataHttpInput,
    DataInput,
    HttpRequest,
    CookiesAuthentication,
    Cookie,
    Authentication,
    DataTransform,
    DataOutput,
)

auths = {}


def config_handle_auth(auth: Authentication):
    auths[auth.id] = auth


def get_auth(key: str) -> Authentication:
    return auths[key]


def run_data_pipeline(pipeline: DataPipeline):
    input_data = run_data_input(pipeline.input)
    data = input_data
    for transformer in pipeline.transforms:
        data = run_data_transform(data, transformer)
    output_data = run_data_output(data, pipeline.output)
    return output_data


def run_data_input(input_config: DataInput):
    if input_config.type == "http":
        return handle_http_input(input_config)
    else:
        raise NotImplementedError(f"Unknown data input type: {input_config.type}")


def run_data_transform(input_data, transform: DataTransform):
    if transform.type == "jq":
        return pyjq.all(transform.jq, input_data)
    else:
        raise NotImplementedError(f"Unknown transform type: {transform.type}")


def run_data_output(data, output: DataOutput):
    print("Output: " + str(data))
    return str(data)


def handle_http_input(data_input: DataHttpInput):
    req = data_input.http
    response = run_http_request(req)
    if data_input.output == "json":
        output = response.json()
    elif data_input.output == "text":
        output = response.text
    elif data_input.output == "response":
        output = response
    else:
        raise NotImplementedError(f"Unknown output type: {data_input.output}")
    return output


def run_http_request(req: HttpRequest):
    if req.authentication:
        auth = req.authentication
        if isinstance(auth, str):
            auth = get_auth(auth)
        if auth.type == "cookies":
            auth: CookiesAuthentication
            session = requests.Session()
            response = run_http_request(auth.http)
            cookies = []
            for cookie in req.authentication.cookies:
                cookies.append(
                    Cookie(
                        name=_render_str(cookie.name, response=response),
                        value=_render_str(cookie.line_edit, response=response),
                    )
                )
            session.cookies.update(cookies)
        else:
            raise NotImplementedError(
                f"Unknown authentication type: {req.authentication.type}"
            )
    else:
        session = requests.Session()

    rendered_headers = {}
    if req.headers:
        for key, value in req.headers.items():
            rendered_headers[jinja2.Template(key).render()] = jinja2.Template(
                value
            ).render()

    data = _render_smart(req.data)
    json_data = _render_smart(req.data)

    response = session.request(
        method=req.method,
        url=jinja2.Template(req.url).render(),
        data=data,
        json=json_data,
        headers=rendered_headers,
    )
    return response


def _render_smart(data, **context):
    if isinstance(data, dict):
        data = _render_dict(data)
    elif isinstance(data, str):
        data = _render_str(data)
    return data


def _render_dict(data: dict, **context):
    rendered = {}
    for key, value in data.items():
        rendered[_render_str(key, **context)] = _render_str(value, **context)
    return rendered


def _render_str(data: str, **context):
    return jinja2.Template(data).render(**context)
