from jmenu.backend.extensions.model import *


extension = Extension(
    id="com.github.jmenu.extensions.fwfin",
    name="FwFin",
    icon="https://fwfin.de/favicon.ico",
    config=Config(
        parameters=[
            UrlConfigParameter(
                name="base_url",
                display_name="Base URL",
                default="https://finance.frachtwerk.de",
                required=True,
            ),
            TextConfigParameter(
                name="username",
                display_name="Username",
                required=True,
            ),
            PasswordConfigParameter(
                name="password",
                display_name="Password",
                required=True,
            ),
        ],
        authentication=TokenAuthentication(
            id="frachtwerk.de:fwfin",
            data_pipeline=DataPipeline(
                input=DataHttpInput(
                    http=HttpRequest(
                        url="{{config.base_url}}/auth/token",
                        method="GET",
                        json={
                            "username": "{{self.config.username}}",
                            "password": "{{self.config.password}}",
                        },
                    ),
                    output="json",
                ),
                transforms=[],
                output=SessionOutput(
                    key="token",
                    value="{{response.token}}",
                ),
            ),
            headers=["Authorization: Bearer {{self.session.token}}"],
        ),
    ),
    matcher=[
        Command(
            name="FwFin",
            command="fwfin",
            aliases=["fw"],
            action=HttpAction(
                http=HttpRequest(
                    url="https://finance.frachtwerk.de/",
                    authentication="frachtwerk.de:fwfin",
                ),
                open_in_browser=True,
            ),
            commands=[
                Command(
                    name="New Entry",
                    command="new",
                    aliases=["n"],
                    arguments=[
                        SelectableArgument(
                            name="project",
                            type="selectable",
                            autocomplete=AutoComplete(
                                data=DataPipeline(
                                    input=DataHttpInput(
                                        http=HttpRequest(
                                            url="https://finance.frachtwerk.de/v1/plans?size=9999",
                                            method="GET",
                                            authentication="frachtwerk.de:fwfin",
                                        ),
                                        output="json",
                                    ),
                                    transforms=[
                                        JqTransform(
                                            jq=".content[] | select(.to | fromdateiso8601 < now)",
                                        )
                                    ],
                                    output=TextOutput(
                                        text="{{item.projectName}}",
                                        value="{{item.id}}",
                                        extra={
                                            "hoursStepSize": "{{item.hoursStepSize}}",
                                        },
                                    ),
                                ),
                            ),
                        ),
                        DurationArgument(
                            name="time",
                            type="duration",
                            validation=ModuloValidation(
                                value="{{arguments.time.to_hours()}}",
                                modulo="{{arguments.project.extra.hoursStepSize}}",
                            ),
                        ),
                        TextArgument(
                            name="description",
                            type="text",
                            optional=True,
                        ),
                        DateArgument(
                            name="date",
                            type="date",
                            default="{{datetime.now()}}",
                        ),
                    ],
                    action=HttpAction(
                        http=HttpRequest(
                            url="https://finance.frachtwerk.de/v1/activities",
                            method="POST",
                            authentication="frachtwerk.de:fwfin",
                            data={
                                "planId": IntField(value="{{arguments.project.value}}"),
                                "date": "{{arguments.date.strftime('%Y-%m-%d')}}",
                                "hours": FloatField(
                                    value="{{arguments.time.to_hours()}}"
                                ),
                                "comment": "{{arguments.description}}",
                                "userId": 0,
                            },
                        ),
                    ),
                ),
            ],
        ),
    ],
)
