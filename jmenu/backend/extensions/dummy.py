from jmenu.backend.extensions.model import *


extension = Extension(
    id="com.github.jmenu.extensions.dummy",
    name="Dummy",
    icon="",
    config=Config(parameters=[]),
    matcher=[
        Command(
            name="Dummy",
            command="dummy",
            aliases=["du"],
            arguments=[
                TextArgument(
                    name="text",
                )
            ],
            action=OpenUrlAction(
                url="https://www.google.com/search?q={{this.arguments.text}}",
            ),
        )
    ],
)
