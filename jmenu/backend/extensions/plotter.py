from typing import Callable

import pyperclip
from PySide6 import QtWidgets, QtCharts, QtGui
from PySide6.QtCore import QKeyCombination, Qt

from jmenu.backend.extensions.model import *
from jmenu.backend.matcher.base import MatcherImpl, MatcherRepository
from jmenu.backend.result import Result, Event, copy_to_clipboard, set_query
from jmenu.backend.search import ScoredResult
from jmenu.events import Query, ActionFinishedEvent, event_bus, SetQueryEvent


class PlotterMatcher(Matcher):
    type: str = "plotter"


extension = Extension(
    id="com.github.jmenu.extensions.plotter",
    name="Plotter",
    icon="",
    config=Config(parameters=[]),
    matcher=[
        PlotterMatcher(),
    ],
)


@MatcherRepository.item_for(PlotterMatcher)
class CalculatorMatcherImpl(MatcherImpl[PlotterMatcher]):
    def __init__(self, model: PlotterMatcher):
        super().__init__(model)

    def match(self, query: Query) -> list[ScoredResult]:
        if not query.query.startswith("plot"):
            return []
        functions = query.argument.split(",")  # e.g. plot x^2, x*3 + 2
        num_points = 100
        min_x = -30
        max_x = 30
        all_functions = []
        for function in functions:
            points = []
            for i in range(num_points):
                x = min_x + i * (max_x - min_x) / num_points
                try:
                    y = eval(function)
                    points.append((x, y))
                except Exception as e:
                    pass
            all_functions.append((function, points))
        return [ScoredResult(result=PlotResult(functions=all_functions), score=100)]


class PlotResult(Result):
    def __init__(self, functions: list[tuple[str, list[tuple[float, float]]]]):
        super().__init__()
        self.functions = functions

    def get_actions(self) -> list[Event]:
        return [
            Event(
                name="copy",
                keys=[
                    QKeyCombination(Qt.CTRL | Qt.Key_C),
                    QKeyCombination(Qt.Key_Return),
                ],
                description="Copy result as image to clipboard",
                do=copy_to_clipboard(str(self.result)),  # TODO: copy as image
            ),
        ]

    def render(self, layout: QtWidgets.QLayout) -> QtWidgets.QWidget:
        container = QtWidgets.QWidget()
        container.setFixedHeight(400)

        chart = QtCharts.QChart()
        for function, points in self.functions:
            series = QtCharts.QLineSeries()
            for x, y in points:
                series.append(x, y)
            chart.addSeries(series)
            chart.legend().markers(series)[0].setLabel(function)

        chart.createDefaultAxes()
        chart.axisX().setRange(-30, 30)
        chart.axisY().setRange(-30, 30)

        chart_view = QtCharts.QChartView(chart)
        chart_view.grab()
        # chart_view.setRenderHint(QtGui.QPainter.Antialiasing)
        container_layout = QtWidgets.QHBoxLayout()
        container_layout.addWidget(chart_view)
        container.setLayout(container_layout)

        layout.addWidget(container)
        return container
