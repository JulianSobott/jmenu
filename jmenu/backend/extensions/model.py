from typing import Union, Literal

from pydantic import BaseModel


class Extension(BaseModel):
    id: str
    name: str
    icon: str
    config: "Config"
    matcher: list["Matcher"]

    def __hash__(self):
        return hash(self.id)


class Config(BaseModel):
    parameters: list["ConfigParameter"]
    authentication: "Authentication" = None


class Authentication(BaseModel):
    type: str
    id: str


class CookiesAuthentication(Authentication):
    type: str = "cookies"
    http: "HttpRequest"
    cookies: list["Cookie"]


class TokenAuthentication(Authentication):
    type: str = "token"
    data_pipeline: "DataPipeline"
    headers: list[str]


class Cookie(BaseModel):
    name: str
    value: str


class BearerAuthentication(Authentication):
    type: str = "bearer"
    token: str


class ConfigParameter(BaseModel):
    name: str
    display_name: str
    type: str
    required: bool
    default: str = None
    description: str = None


class UrlConfigParameter(ConfigParameter):
    type: str = "url"


class TextConfigParameter(ConfigParameter):
    type: str = "text"


class PasswordConfigParameter(ConfigParameter):
    type: str = "password"


class Matcher(BaseModel):
    type: str


class Command(Matcher):
    type: str = "keyword"
    name: str
    command: str
    aliases: list[str] = []
    arguments: list["Argument"] = []
    action: "Action"
    commands: list["Command"] = []


class Action(BaseModel):
    type: str


class NoAction(Action):
    type: str = "no_action"


class HttpAction(Action):
    type: str = "http"
    http: "HttpRequest"
    open_in_browser: bool = False


class OpenUrlAction(Action):
    type: str = "open_url"
    url: str


class ShowResultAction(Action):
    type: str = "show_result"
    result: "DataPipeline"


class Argument(BaseModel):
    name: str
    type: str
    optional: bool = False
    autocomplete: "AutoComplete" = None
    default: str = None
    validation: "Validation" = None


class TextArgument(Argument):
    type: str = "text"


class SelectableArgument(Argument):
    type: str = "selectable"
    autocomplete: "AutoComplete"


class DurationArgument(Argument):
    type: str = "duration"


class DateArgument(Argument):
    type: str = "date"
    format: str = "%Y-%m-%d"
    default: str = "{{datetime.now()}}"


class Validation(BaseModel):
    type: str


class ModuloValidation(Validation):
    type: str = "modulo"
    value: str
    modulo: str


class AutoComplete(BaseModel):
    data: "DataPipeline"


class DataInput(BaseModel):
    type: str


class DataHttpInput(DataInput):
    type: str = "http"
    http: "HttpRequest"
    output: Literal["json", "text", "response"]


class DataListInput(DataInput):
    type: str = "list"
    data: list[object]


class DataTransform(BaseModel):
    type: str


class JsonParser(DataTransform):
    type: str = "json_parser"


class JqTransform(DataTransform):
    type: str = "jq"
    jq: str


class DataOutput(BaseModel):
    type: str


class TextOutput(DataOutput):
    type: str = "text"
    text: str
    value: str
    extra: dict[str, object] = None


class SessionOutput(DataOutput):
    type: str = "session"
    key: str
    value: str


class DataPipeline(BaseModel):
    input: "DataInput"
    transforms: list["DataTransform"]
    output: "DataOutput"


class HttpRequest(BaseModel):
    url: str
    method: str = "GET"
    headers: dict[str, str] = None
    data: str | dict = None
    json: str | dict = None
    params: dict[str, str] = None
    authentication: Union[str, "Authentication"] = None


class Field(BaseModel):
    type: str
    value: str

    # _field_implementations: ClassVar[dict[str, Type["Field"]]] = {}

    # @model_validator(mode="after")
    # def register_type(self):
    #     Field._field_implementations[self.type] = self.__class__
    #     print(Field._field_implementations)
    #
    # @classmethod
    # def get_handler(cls, type_: str) -> Type["Field"]:
    #     print(Field._field_implementations)
    #     return cls._field_implementations[type_]


class IntField(Field):
    type: str = "int"


class FloatField(Field):
    type: str = "float"
