from jmenu.backend.extensions.model import *


extension = Extension(
    id="com.github.jmenu.extensions.hedgedoc",
    name="Hedgedoc",
    icon="https://notes.frachtwerk.de/banner/banner_vertical_color.svg",
    config=Config(
        parameters=[
            UrlConfigParameter(
                name="base_url",
                display_name="Base URL",
                default="https://notes.frachtwerk.de",
                required=True,
            ),
            TextConfigParameter(
                name="username",
                display_name="Username",
                required=True,
            ),
            PasswordConfigParameter(
                name="password",
                display_name="Password",
                required=True,
            ),
        ],
        # authentication: POST {{config.base_url}}/auth/ldap with username, password as form data; authentication Cookie connect.sid is set
        authentication=CookiesAuthentication(
            id="frachtwerk.de:hedgedoc",
            http=HttpRequest(
                url="{{config.base_url}}/auth/ldap",
                method="POST",
                data={
                    "username": "{{arguments.username}}",
                    "password": "{{arguments.password}}",
                },
            ),
            cookies=[
                Cookie(
                    name="connect.sid",
                    value="{{response.cookies['connect.sid']}}",
                ),
            ],
        ),
    ),
    matcher=[
        Command(
            name="Hedgedoc",
            command="hedgedoc",
            aliases=["hd"],
            action=HttpAction(
                http=HttpRequest(
                    url="{{config.base_url}}",
                    authentication="frachtwerk.de:hedgedoc",
                ),
            ),
            commands=[
                Command(
                    name="New Document",
                    command="new",
                    aliases=["n"],
                    arguments=[
                        Argument(
                            name="title",
                            type="text",
                            optional=True,
                        ),
                    ],
                    action=HttpAction(
                        http=HttpRequest(
                            url="{{config.base_url}}/new",
                            method="POST",
                            data="{{'# ' + arguments.title.encode('utf-8') if arguments.title else '' }}",
                            headers={"Content-Type": "text/markdown; charset=UTF-8"},
                            authentication="frachtwerk.de:hedgedoc",
                        ),
                    ),
                ),
                Command(
                    name="Open Document",
                    command="open",
                    aliases=["o"],
                    arguments=[
                        Argument(
                            name="id",
                            type="text",
                            autocomplete=AutoComplete(
                                data=DataPipeline(
                                    input=DataHttpInput(
                                        http=HttpRequest(
                                            url="{{config.base_url}}/history",
                                            authentication="frachtwerk.de:hedgedoc",
                                        ),
                                        output="json",
                                    ),
                                    transforms=[
                                        JqTransform(
                                            jq="history[]",
                                        ),
                                    ],
                                    output=TextOutput(
                                        text="{{item.text}}",
                                        value="{{item.id}}",
                                    ),
                                )
                            ),
                        ),
                    ],
                    action=HttpAction(
                        http=HttpRequest(
                            url="{{config.base_url}}/{{arguments.id}}",
                            authentication="frachtwerk.de:hedgedoc",
                        )
                    ),
                ),
                Command(
                    name="Count Documents",
                    command="count",
                    aliases=["c"],
                    arguments=[
                        Argument(
                            name="filter",
                            type="text",
                            optional=True,
                        )
                    ],
                    action=ShowResultAction(
                        result=DataPipeline(
                            input=DataHttpInput(
                                http=HttpRequest(
                                    url="{{config.base_url}}/history",
                                    authentication="frachtwerk.de:hedgedoc",
                                ),
                                output="json",
                            ),
                            transforms=[
                                JqTransform(
                                    jq="history[] | select(.text | contains({{arguments.filter if arguments.filter else '' }})) | length",
                                )
                            ],
                            output=TextOutput(
                                text="{{ item }} Documents",
                                value="{{item}}",
                            ),
                        ),
                    ),
                ),
            ],
        )
    ],
)
