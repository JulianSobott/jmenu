from jmenu.backend.extensions.model import *


extension = Extension(
    id="com.github.jmenu.extensions.gitlab",
    name="GitLab",
    icon="https://fwfin.de/favicon.ico",
    config=Config(
        parameters=[
            UrlConfigParameter(
                name="base_url",
                display_name="Base URL",
                required=True,
                default="https://gitlab.com",
            ),
            TextConfigParameter(
                name="username", display_name="Username", required=True
            ),
            PasswordConfigParameter(name="token", display_name="Token", required=True),
        ],
        authentication=TokenAuthentication(
            id="frachtwerk.de:gitlab",
            token="{{config.token}}",
        ),
    ),
    commands=[
        Command(
            name="GitLab",
            command="gitlab",
            aliases=["gl"],
            action=HttpAction(
                http=HttpRequest(
                    url="{{config.base_url}}",
                ),
                open_in_browser=True,
            ),
            commands=[
                Command(
                    name="Open",
                    command="open",
                    aliases=[
                        "o"
                    ],  # idea: open {type:pipeline, (my) issues} {project:selectable}
                    arguments=[
                        Argument(
                            name="type",
                            type="selectable",
                            autocomplete=AutoComplete(
                                data=DataPipeline(
                                    input=DataListInput(
                                        data=[
                                            {
                                                "name": "Issues",
                                                "value": "issues",
                                                "url": "boards",
                                                "filter": "",
                                            },
                                            {
                                                "name": "My Issues",
                                                "value": "my_issues",
                                                "url": "boards",
                                                "filter": "assignee_username[]={{config.username}}",
                                            },
                                            {
                                                "name": "Pipelines",
                                                "value": "pipelines",
                                                "url": "pipelines",
                                            },
                                        ]
                                    ),
                                    transforms=[],
                                    output=TextOutput(
                                        text="{{item.name}}",
                                        value="{{item.value}}",
                                    ),
                                ),
                            ),
                        ),
                        Argument(
                            name="project",
                            type="string",
                            autocomplete=AutoComplete(
                                data=DataPipeline(
                                    input=DataHttpInput(
                                        http=HttpRequest(
                                            url="{{config.base_url}}/api/v4/projects",
                                            method="GET",
                                            authentication="frachtwerk.de:gitlab",
                                        ),
                                        output="json",
                                    ),
                                    transforms=[
                                        JqTransform(
                                            jq=".[] | select(.archived == false)"
                                        )
                                    ],
                                    output=TextOutput(
                                        text="{{item.name}}",
                                        value="{{item.id}}",
                                    ),
                                ),
                            ),
                        ),
                    ],
                    action=HttpAction(
                        http=HttpRequest(
                            url="{{config.base_url}}/{{arguments.project}}/{{arguments.type.url}}"
                            "?{{arguments.type.filter}}",
                        ),
                        open_in_browser=True,
                    ),
                ),
            ],
        ),
    ],
)
