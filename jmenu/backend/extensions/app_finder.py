from jmenu.backend.extensions.model import *


class AppFinderMatcher(Matcher):
    type: str = "app_finder"


extension = Extension(
    id="com.github.jmenu.extensions.app_finder",
    name="App Finder",
    icon="https://jmenu.github.io/static/images/logo.png",
    config=Config(parameters=[]),
    matcher=[
        AppFinderMatcher(),
    ],
)
