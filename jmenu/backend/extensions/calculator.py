from PySide6 import QtWidgets
from PySide6.QtCore import QKeyCombination, Qt
from py_expression_eval import Parser

from jmenu.backend.extensions.model import *
from jmenu.backend.matcher.base import MatcherImpl, MatcherRepository
from jmenu.backend.result import Result, Event, copy_to_clipboard, set_query
from jmenu.backend.search import ScoredResult
from jmenu.events import Query


class CalculatorMatcher(Matcher):
    type: str = "calculator"


extension = Extension(
    id="com.github.jmenu.extensions.calculator",
    name="Calculator",
    icon="",
    config=Config(parameters=[]),
    matcher=[CalculatorMatcher()],
)


@MatcherRepository.item_for(CalculatorMatcher)
class CalculatorMatcherImpl(MatcherImpl[CalculatorMatcher]):
    def __init__(self, model: CalculatorMatcher):
        super().__init__(model)
        self.parser = Parser()
        self.last_result = None
        self.last_expression = None

    def match(self, query: Query) -> list[ScoredResult]:
        if not query.query.startswith("="):
            return []
        try:
            expression = query.query[1:]
            result = self.parser.parse(expression).evaluate({})
            self.last_result = result
            self.last_expression = expression
            return [ScoredResult(result=MathResult(expression, result), score=100)]
        except Exception as e:
            if self.last_result is not None:
                return [
                    ScoredResult(
                        result=MathResult(self.last_expression, self.last_result),
                        score=100,
                    )
                ]
            return []


class MathResult(Result):
    def __init__(self, expression, result):
        super().__init__()
        self.result = result
        self.expression = expression

    def get_actions(self) -> list[Event]:
        return [
            Event(
                name="copy",
                keys=[
                    QKeyCombination(Qt.CTRL | Qt.Key_C),
                    QKeyCombination(Qt.Key_Return),
                ],
                description="Copy result to clipboard",
                do=copy_to_clipboard(str(self.result)),
            ),
            Event(
                name="fill in",
                keys=[QKeyCombination(Qt.Key_Tab)],
                description="Fill in result",
                do=set_query(self.on_fill_in()),
            ),
        ]

    def render(self, layout: QtWidgets.QLayout) -> QtWidgets.QWidget:
        container = QtWidgets.QWidget()
        container.setStyleSheet("QWidget { background-color: #333333; }")

        lbl_expression = QtWidgets.QLabel(str(self.expression) + " = ")
        lbl_expression.setStyleSheet("QLabel { color : #aaa; font-size: 14px; }")

        lbl_result = QtWidgets.QLabel(str(self.result))
        lbl_result.setStyleSheet("QLabel { color : white; font-size: 20px; }")

        container_layout = QtWidgets.QHBoxLayout()
        container_layout.addWidget(lbl_expression)
        container_layout.addWidget(lbl_result)

        container.setLayout(container_layout)
        layout.addWidget(container)
        return container

    def on_fill_in(self) -> str:
        return "=" + str(self.result)
