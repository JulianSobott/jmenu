import logging
from collections import defaultdict
from dataclasses import dataclass
from typing import Callable

from jmenu.backend.extensions.model import Extension, Command


class Query:
    def __init__(self, query: str):
        self.query = query
        self.keyword, self.argument = self._get_components()

    def _get_components(self):
        return self.query.split(" ", 1) if " " in self.query else (self.query, "")

    def __repr__(self):
        return f"Query(keyword={self.keyword}, argument={self.argument})"


class BaseEvent:
    pass


class KeyboardEvent(BaseEvent):
    pass


@dataclass
class QueryChangedEvent(BaseEvent):
    query: Query


@dataclass
class ExtensionSelectedEvent(BaseEvent):
    extension_id: str


@dataclass
class CommandSelectedEvent(BaseEvent):
    extension: Extension
    command: "Command"


@dataclass
class AppSelectedEvent(BaseEvent):
    app_id: str


@dataclass
class AppDeselectedEvent(BaseEvent):
    app_id: str


@dataclass
class ExtensionDeselectedEvent(BaseEvent):
    extension_id: str


@dataclass
class KeywordChangedEvent(BaseEvent):
    keyword: str


@dataclass
class ArgumentChangedEvent(BaseEvent):
    argument: str


@dataclass
class ResultsChangedEvent(BaseEvent):
    results: list["ScoredResult"]


@dataclass
class ResultSelectedEvent(BaseEvent):
    result: "ScoredResult"


@dataclass
class ActionFinishedEvent(BaseEvent):
    pass


class StartupEvent(BaseEvent):
    pass


@dataclass
class CommandChangedEvent(BaseEvent):
    command: "Command"


@dataclass
class ArgumentMatchedEvent(BaseEvent):
    argument: "Argument"
    matched: object


@dataclass
class NextArgumentEvent(BaseEvent):
    next_argument: "Argument"


class HighlightingChangedEvent(BaseEvent):
    pass


@dataclass
class AutocompleteValuesChangedEvent(BaseEvent):
    values: list["Result"]


@dataclass
class ExtensionRegisteredEvent(BaseEvent):
    extension: Extension


@dataclass
class SetQueryEvent(BaseEvent):
    query: Query


@dataclass
class SetArgumentMatchingModeEvent(BaseEvent):
    is_argument_matching_mode: bool
    matcher: "ArgumentMatcher"
    query: Query


class EventBus:
    def __init__(self):
        self.subscribers = defaultdict(list)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

    def subscribe(self, event_type: type[BaseEvent], listener: Callable) -> None:
        self.subscribers[event_type].append(listener)
        self.logger.debug(f"Subscribed {listener} to {event_type.__name__}")

    def unsubscribe(self, event_type: type[BaseEvent], listener: Callable) -> None:
        self.subscribers[event_type].remove(listener)
        self.logger.debug(f"Unsubscribed {listener} from {event_type.__name__}")

    def publish(self, event: BaseEvent) -> None:
        self.logger.debug(f"Publishing {event}")
        for subscriber in self.subscribers[type(event)]:
            try:
                subscriber(event)
            except Exception as e:
                self.logger.exception(e)
            self.logger.debug(f"|-Published {event} to {subscriber}")


event_bus = EventBus()
