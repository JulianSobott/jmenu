import logging

from PySide6 import QtWidgets, QtGui, QtCore

from jmenu.backend.config import config
from jmenu.backend.extensions.model import (
    Extension,
    UrlConfigParameter,
    PasswordConfigParameter,
    TextConfigParameter,
    ConfigParameter,
)

logger = logging.getLogger(__name__)


class ExtensionWidget(QtWidgets.QWidget):
    def __init__(self, extension: Extension):
        super().__init__()
        self.extension = extension


class WidgetsRepository:
    widgets = {}

    @classmethod
    def widget_for(cls, model: type):
        def decorator(widget):
            logger.info(f"Registering widget {widget} for model {model}")
            cls.widgets[model] = widget
            return widget

        return decorator

    @classmethod
    def get_widget_for(cls, model: type):
        def not_found(model_type: type):
            def widget(extension: Extension, **kwargs):
                return NotFoundWidget(model_type, extension)

            return widget

        return cls.widgets.get(model, not_found(model))


class TextValueConfig(ExtensionWidget):
    def __init__(self, extension: Extension, parameter: ConfigParameter):
        super().__init__(extension)
        self.parameter = parameter

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.line_edit = QtWidgets.QLineEdit()
        validator = self.get_validator()
        if validator:
            self.line_edit.setValidator(validator)
        value = config.get_value_for(extension, parameter.name) or parameter.default
        self.line_edit.setText(value)
        self.layout.addWidget(self.line_edit)

    def get_validator(self) -> QtGui.QValidator | None:
        return None

    def save(self):
        config.set_value_for(self.extension, self.parameter.name, self.line_edit.text())


@WidgetsRepository.widget_for(UrlConfigParameter)
class UrlWidget(TextValueConfig):
    def __init__(self, extension: Extension, parameter: UrlConfigParameter):
        super().__init__(extension, parameter)

    def get_validator(self):
        return QtGui.QRegularExpressionValidator(
            QtCore.QRegularExpression(r"https?://.*")
        )


@WidgetsRepository.widget_for(TextConfigParameter)
class TextParameterWidget(TextValueConfig):
    def __init__(self, extension: Extension, parameter: TextConfigParameter):
        super().__init__(extension, parameter)

    def get_validator(self):
        if self.parameter.required:
            return RequiredValidator()
        else:
            return None


@WidgetsRepository.widget_for(PasswordConfigParameter)
class PasswordParameterWidget(TextValueConfig):
    def __init__(self, extension: Extension, parameter: PasswordConfigParameter):
        super().__init__(extension, parameter)
        password = config.get_secret_for(extension, parameter.name) or parameter.default
        self.line_edit.setText(password)
        self.line_edit.setEchoMode(QtWidgets.QLineEdit.Password)

    def get_validator(self):
        if self.parameter.required:
            return RequiredValidator()
        else:
            return None

    def save(self):
        config.set_secret_for(
            self.extension, self.parameter.name, self.line_edit.text()
        )


class RequiredValidator(QtGui.QValidator):
    def validate(self, in_value: str, pos: int):
        if in_value:
            return self.State.Acceptable, in_value, pos
        else:
            return self.State.Invalid, in_value, pos


class NotFoundWidget(ExtensionWidget):
    def __init__(self, model_type: type, extension: Extension):
        super().__init__(extension)

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(
            QtWidgets.QLabel(
                "Widget Not found for model type: "
                + str(model_type)
                + " in extension: "
                + extension.name
            )
        )

        self.layout.addStretch()
