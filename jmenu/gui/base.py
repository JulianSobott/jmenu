from abc import abstractmethod

from jmenu.backend.extensions.model import Command
from jmenu.events import (
    QueryChangedEvent,
    event_bus,
    Query,
    ResultsChangedEvent,
    ActionFinishedEvent,
    CommandChangedEvent,
    AutocompleteValuesChangedEvent,
    CommandSelectedEvent,
    SetQueryEvent,
)


class Gui:
    def __init__(self):
        event_bus.subscribe(ResultsChangedEvent, self.on_results_changed)
        event_bus.subscribe(ActionFinishedEvent, self._on_action_finished)
        event_bus.subscribe(CommandSelectedEvent, self._on_command_selected)
        event_bus.subscribe(CommandChangedEvent, self._on_command_changed)
        event_bus.subscribe(
            AutocompleteValuesChangedEvent, self._on_autocomplete_values_changed
        )
        event_bus.subscribe(SetQueryEvent, self._on_set_query)
        self.original_query = None

    @abstractmethod
    def mainloop(self):
        raise NotImplementedError

    def _on_query_changed(self, query: str) -> None:
        if query == self.original_query:
            # preventing infinite loop, triggered through a (syntax) highlight of the query
            return
        self.original_query = query
        event_bus.publish(QueryChangedEvent(Query(query)))

    def _on_command_selected(self, event: CommandSelectedEvent) -> None:
        self.show_command_preview(event.command)

    def _on_command_changed(self, event: CommandChangedEvent) -> None:
        command = event.command
        self.update_command_preview(command)

    def _on_autocomplete_values_changed(
        self, event: AutocompleteValuesChangedEvent
    ) -> None:
        self.update_autocomplete_values(event.values)

    def _on_set_query(self, event: SetQueryEvent) -> None:
        self.set_query(event.query.query)

    @abstractmethod
    def update_autocomplete_values(self, values: list["Result"]) -> None:
        raise NotImplementedError

    @abstractmethod
    def on_results_changed(self, event: ResultsChangedEvent) -> None:
        raise NotImplementedError

    def _on_action_finished(self, event: ActionFinishedEvent) -> None:
        self.close()

    @abstractmethod
    def close(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def show_command_preview(self, command: Command):
        raise NotImplementedError

    @abstractmethod
    def hide_command_preview(self):
        raise NotImplementedError

    @abstractmethod
    def update_command_preview(self, command: Command):
        raise NotImplementedError

    @abstractmethod
    def set_query(self, query: str):
        raise NotImplementedError
