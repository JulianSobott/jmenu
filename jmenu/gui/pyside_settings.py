import logging

from PySide6 import QtGui
from PySide6 import QtWidgets

from jmenu.backend.backend import extension_repository
from jmenu.backend.extensions.model import (
    Extension,
)
from jmenu.gui.widgets import WidgetsRepository

logger = logging.getLogger(__name__)


class SettingsWidget(QtWidgets.QWidget):
    def __init__(self, navigate_back: callable):
        super().__init__()

        # state
        self.savable_widgets = []

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        back = QtWidgets.QPushButton("Back")
        back.clicked.connect(navigate_back)
        self.layout.addWidget(back)

        self.layout.addWidget(QtWidgets.QLabel("Settings"))

        # split view: on the left side, show the list of extensions
        # on the right side, show the settings for the selected extension
        self.split_view = QtWidgets.QSplitter()
        self.layout.addWidget(self.split_view)

        self.extension_list = self.create_extension_list()
        self.split_view.addWidget(self.extension_list)

        self.extension_settings_layout = QtWidgets.QVBoxLayout()
        self.extension_settings_layout.setStretch(1, 1)
        self.extension_settings = QtWidgets.QWidget()
        self.extension_settings.setLayout(self.extension_settings_layout)
        self.split_view.addWidget(self.extension_settings)
        self.split_view.setSizes([200, self.width() - 200])

        self.layout.addStretch()

    def create_extension_list(self):
        extension_list = QtWidgets.QListWidget()
        extensions = extension_repository.get_extensions()
        for extension in extensions:
            item = ExtensionItem(extension)
            extension_list.addItem(item)
        extension_list.itemSelectionChanged.connect(self.on_extension_selected)
        return extension_list

    def on_extension_selected(self):
        self.savable_widgets = []
        extension = self.extension_list.currentItem().extension
        extension_settings = self.create_extension_settings(extension)
        # remove all widgets from the layout
        for i in reversed(range(self.extension_settings_layout.count())):
            self.extension_settings_layout.itemAt(i).widget().setParent(None)
        self.extension_settings_layout.addWidget(extension_settings)

    def create_extension_settings(self, extension: Extension):
        extension_settings = QtWidgets.QWidget()
        layout = QtWidgets.QVBoxLayout()
        extension_settings.setLayout(layout)
        extension_label = QtWidgets.QLabel(extension.name)
        extension_label.setFont(QtGui.QFont("Arial", 20))
        layout.addWidget(extension_label)

        for param in extension.config.parameters:
            param_layout = QtWidgets.QHBoxLayout()
            param_layout.addWidget(QtWidgets.QLabel(param.display_name))
            widget = WidgetsRepository.get_widget_for(type(param))(
                extension, parameter=param
            )
            self.savable_widgets.append(widget)
            param_layout.addWidget(widget)
            layout.addLayout(param_layout)

        btn_save = QtWidgets.QPushButton("Save")
        btn_save.clicked.connect(lambda: self.save())
        layout.addWidget(btn_save)
        return extension_settings

    def save(self):
        for widget in self.savable_widgets:
            widget.save()


class ExtensionItem(QtWidgets.QListWidgetItem):
    def __init__(self, extension: Extension, **kwargs):
        super().__init__(extension.name)
        self.extension = extension
        self.kwargs = kwargs
