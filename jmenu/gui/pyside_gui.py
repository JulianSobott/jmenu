import logging
import sys
from dataclasses import dataclass
from typing import Callable

from PySide6 import QtWidgets, QtGui, QtCore
from PySide6.QtCore import (
    Qt,
    QEvent,
)
from PySide6.QtGui import QKeyEvent, QSyntaxHighlighter
from PySide6.QtWidgets import QApplication

from jmenu.backend.arguments.argument import Argument
from jmenu.backend.backend import extension_repository
from jmenu.backend.extensions.model import Command
from jmenu.backend.result import Result, Context
from jmenu.events import (
    ResultsChangedEvent,
    event_bus,
    StartupEvent,
    ExtensionSelectedEvent,
    HighlightingChangedEvent,
    CommandChangedEvent,
    Query,
)
from jmenu.gui.base import Gui
from jmenu.gui.pyside_settings import SettingsWidget


class PysideGui(Gui):
    def __init__(self):
        super().__init__()
        self.gui = None
        self.max_results = 10
        self.min_score = 40

    def mainloop(self):
        app = QtWidgets.QApplication([])
        widget = MainWidget(self._on_query_changed)
        widget.resize(800, 600)

        # center the window on the screen
        frame_geometry = widget.frameGeometry()
        cursor = QtGui.QCursor()
        cursor_pos = cursor.pos()
        center_point = (
            QtGui.QGuiApplication.screenAt(cursor_pos).availableGeometry().center()
        )
        frame_geometry.moveCenter(center_point)
        widget.move(frame_geometry.topLeft())

        self.gui = widget

        widget.show()
        event_bus.publish(StartupEvent())
        sys.exit(app.exec_())

    def on_results_changed(self, event: ResultsChangedEvent) -> None:
        results = [
            r.result
            for r in event.results[: self.max_results]
            if r.score > self.min_score
        ]
        self.gui.set_results(results)

    def show_command_preview(self, command: Command):
        self.gui.show_command_preview(command)

    def hide_command_preview(self):
        self.gui.hide_command_preview()

    def update_command_preview(self, command: Command):
        self.gui.update_command_preview(command)

    def update_autocomplete_values(self, values: list["Result"]) -> None:
        self.gui.completion_model.setStringList([r.text for r in values])

    def set_query(self, query: str) -> None:
        self.gui.text_box.setText(query)
        self.gui.text_box.setFocus()
        self.gui.text_box.moveCursor(QtGui.QTextCursor.MoveOperation.EndOfLine)

    def _on_query_changed(self, text: str) -> None:
        logging.info(f"Query changed: '{text}'")
        super()._on_query_changed(text)

    def close(self) -> None:
        self.gui.close()


class MainWidget(QtWidgets.QWidget):
    def __init__(self, on_text_changed: Callable[[str], None], parent=None):
        super(MainWidget, self).__init__(parent)

        self.logger = logging.getLogger(__name__)

        # state
        self.selected_index = -1
        self.results: list[Result] = []
        self.command: Command = None

        # StackView
        self.stack = QtWidgets.QStackedWidget()

        # build the stack
        root = QtWidgets.QWidget()
        self.stack.addWidget(root)
        self.stack.setCurrentIndex(0)
        self.stack.show()

        settings = SettingsWidget(lambda: self.stack.setCurrentIndex(0))
        self.settings_view_index = self.stack.addWidget(settings)

        # window

        self.setWindowFlags(
            Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.Dialog
        )

        self.layout = QtWidgets.QVBoxLayout(self)

        # input box
        # self.text_box = TextEdit(self)
        self.text_box = QtWidgets.QTextEdit()
        self.text_box.setFont(QtGui.QFont("Arial", 20))
        self.text_box.textChanged.connect(
            lambda: on_text_changed(self.text_box.toPlainText())
        )
        self.text_box.setFixedHeight(40)
        self.text_box.setLineWrapMode(QtWidgets.QTextEdit.LineWrapMode.NoWrap)
        # hide scrollbars
        self.text_box.setHorizontalScrollBarPolicy(
            Qt.ScrollBarPolicy.ScrollBarAlwaysOff
        )
        self.text_box.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.layout.addWidget(self.text_box)

        # highlighting
        Highlighter(HighlightingState(), self.text_box.document())

        # command preview box (hidden by default, but space is reserved)
        self.command_preview_layout = QtWidgets.QHBoxLayout()
        self.command_preview_layout.setAlignment(Qt.AlignLeft)
        self.command_preview_widget = QtWidgets.QWidget()
        self.command_preview_widget.setLayout(self.command_preview_layout)
        self.command_preview_widget.setFixedHeight(22)
        self.command_preview_layout.setContentsMargins(0, 0, 0, 0)

        self.layout.addWidget(self.command_preview_widget)

        # results
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setHorizontalScrollBarPolicy(
            Qt.ScrollBarPolicy.ScrollBarAlwaysOff
        )

        self.results_widget = QtWidgets.QWidget()
        self.results_layout = QtWidgets.QVBoxLayout()
        self.results_layout.setAlignment(Qt.AlignTop)

        self.results_widget.setLayout(self.results_layout)

        self.scroll_area.setWidget(self.results_widget)
        self.layout.addWidget(self.scroll_area)

        # footer container with buttons: settings
        self.footer_widget = QtWidgets.QWidget()
        self.footer_layout = QtWidgets.QHBoxLayout()
        self.footer_layout.setAlignment(Qt.AlignRight)
        self.footer_widget.setLayout(self.footer_layout)
        self.footer_widget.setFixedHeight(22)
        self.footer_layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.footer_widget)

        # footer buttons
        self.settings_button = QtWidgets.QPushButton()
        self.settings_button.clicked.connect(
            lambda: self.stack.setCurrentIndex(self.settings_view_index)
        )
        self.settings_button.setFlat(True)
        pixmap = QtGui.QPixmap("../resources/icons/settings.png")
        self.settings_button.setIcon(QtGui.QIcon(pixmap))
        self.settings_button.setIconSize(QtCore.QSize(18, 18))
        self.footer_layout.addWidget(self.settings_button)

        # set layout
        root.setLayout(self.layout)
        root_layout = QtWidgets.QVBoxLayout()
        root_layout.addWidget(self.stack)
        self.setLayout(root_layout)

        # debug
        # self.stack.setCurrentIndex(self.settings_view_index)

        # intercept (Keyboard) events
        self.text_box.installEventFilter(self)
        self.results_widget.installEventFilter(self)

    def childEvent(self, event):
        # recursively installEventFilter
        if event.type() == QtCore.QEvent.Type.ChildAdded:
            if event.child().isWidgetType():
                event.child().installEventFilter(self)
        elif event.type() == QtCore.QEvent.Type.ChildRemoved:
            if event.child().isWidgetType():
                event.child().removeEventFilter(self)
        return super().childEvent(event)

    def eventFilter(self, source, event: QEvent) -> bool:
        # TODO: deactivate event filter when settings are open
        # self.logger.info(f"Event: {event}")
        if event.type() == QEvent.Type.KeyPress:
            if event.key() == Qt.Key_Tab:
                # self._fill_in_selected_result(self.selected_index)
                return True
            elif event.key() in (Qt.Key_Enter, Qt.Key_Return):
                # self._select_result(self.selected_index)
                return True
            elif event.key() == Qt.Key_Down:
                self._down_pressed()
                return True
            elif event.key() == Qt.Key_Up:
                self._up_pressed()
                return True
        elif event.type() == QEvent.Type.ShortcutOverride:
            self.logger.info(
                f"key sequence: {event.keyCombination()} | {event.modifiers()}"
            )
            return self._handle_shortcut(event)
        return super().eventFilter(source, event)

    def show_command_preview(self, command: Command):
        self._build_command_preview(command)

    def hide_command_preview(self):
        while item := self.command_preview_layout.takeAt(0):
            item.widget().deleteLater()

    def update_command_preview(self, command: Command):
        self.command = command
        self.show_command_preview(command)

    def _build_command_preview(self, command: Command):
        self.hide_command_preview()

        label_pre = QtWidgets.QLabel(">")
        label_pre.setFont(QtGui.QFont("Arial", 14))
        label_pre.setStyleSheet("QWidget { color: #333333; }")

        label_extension = QtWidgets.QLabel(command.name)
        label_extension.setFont(QtGui.QFont("Arial", 16))
        label_extension.setStyleSheet("QWidget { color: #2B2B2B; font-weight: bold; }")

        self.command_preview_layout.addWidget(label_pre)
        self.command_preview_layout.addWidget(label_extension)

        for i, command in enumerate(command.commands):
            label_command = QtWidgets.QLabel(command.name)
            label_command.setFont(QtGui.QFont("Arial", 14))
            # if i == command.current_arg_index:
            #     label_command.setStyleSheet(
            #         "QWidget { color: #133137; font-weight: bold;}"
            #     )
            # else:
            #     label_command.setStyleSheet("QWidget { color: #133137; }")
            self.command_preview_layout.addWidget(label_command)

        for i, arg in enumerate(command.arguments):
            text = f"{arg.name}={format_value(arg.line_edit)}"
            label_args = QtWidgets.QLabel(text)
            label_args.setFont(QtGui.QFont("Arial", 14))
            # if i == command.current_arg_index:
            #     label_args.setStyleSheet(
            #         "QWidget { color: #133137; font-weight: bold;}"
            #     )
            # else:
            #     label_args.setStyleSheet("QWidget { color: #133137; }")
            self.command_preview_layout.addWidget(label_args)

    def set_results(self, results: list[Result]):
        while item := self.results_layout.takeAt(0):
            item.widget().deleteLater()
        for result in results:
            container = result.render(self.results_layout)

        self.selected_index = 0
        self._highlight_result(self.selected_index)
        self.results = results

    def _handle_shortcut(self, event: QKeyEvent):
        if 0 <= self.selected_index and len(self.results) > 0:
            ctx = Context(query=Query(self.text_box.toPlainText()))
            keys: QtCore.QKeyCombination = event.keyCombination()
            current_result = self.results[self.selected_index]
            for action in current_result.get_actions():
                for key in action.keys:
                    if keys.toCombined() == key.toCombined():
                        action.do(current_result, ctx)
                        self.logger.info(f"Action {action.name} triggered")
                        return False
        return True

    def _highlight_result(self, index: int):
        for i in range(self.results_layout.count()):
            widget = self.results_layout.itemAt(i).widget()
            if i == index:
                widget.setStyleSheet("QWidget { background-color: #0044ff; }")
                self.scroll_area.ensureWidgetVisible(widget)
            else:
                widget.setStyleSheet("QWidget { background-color: #133137; }")

    def _select_result(self, index: int):
        pass
        # self.command.action(Action(), self.command)
        # widget = self.results_layout.itemAt(index).widget()
        # widget.setStyleSheet("QWidget { background-color: #00ff00; }")
        # self.results[index].on_selected()
        # event_bus.publish(ResultSelectedEvent(self.results[index]))

    def _down_pressed(self):
        self.selected_index = min(
            self.selected_index + 1, self.results_layout.count() - 1
        )
        self._highlight_result(self.selected_index)

    def _up_pressed(self):
        self.selected_index = max(self.selected_index - 1, 0)
        self._highlight_result(self.selected_index)

    # def _fill_in_selected_result(self, index: int):
    #     self.logger.info(f"fill in result {index}")
    #     if index >= len(self.results):
    #         return
    #     result = self.results[index]
    #     prefix = result.on_fill_in()
    #     self.text_box.setText(prefix + " ")
    #     self.text_box.setFocus()
    #     self.text_box.moveCursor(QtGui.QTextCursor.MoveOperation.EndOfLine)
    # self.text_box.insertPlainText(" ")

    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() == Qt.Key_Escape:
            QApplication.quit()


def format_value(value):
    if isinstance(value, (str, int, float, bool, type(None))):
        formatted = value
    else:
        formatted = repr(value)
    try:
        if (
            " " in formatted
            or '"' in formatted
            or "\n" in formatted
            or "\t" in formatted
            or "=" in formatted
        ):
            formatted = formatted.replace('"', r"\"")
            formatted = f'"{formatted}"'

    except Exception:
        logging.warning(f"Failed to format value: {value}")
    return formatted


class HighlightingState:
    @dataclass
    class Block:
        start: int
        end: int
        color: QtGui.QColor

    def __init__(self):
        self.blocks = []
        self.previous_command = None
        self.query_start = 0
        event_bus.subscribe(ExtensionSelectedEvent, self.on_extension_selected)
        event_bus.subscribe(CommandChangedEvent, self.on_command_changed)

    def on_extension_selected(self, event: ExtensionSelectedEvent):
        name = extension_repository.get_extension(event.extension_id).get_command().name
        self.blocks.append(self.Block(0, len(name), QtGui.QColor(255, 0, 0)))
        event_bus.publish(HighlightingChangedEvent())
        self.query_start = len(name) + 1

    def on_command_changed(self, event: CommandChangedEvent):
        self.blocks = [self.blocks[0]]
        for arg in event.command.args:
            arg: Argument
            if arg.value:
                color = arg.runtime_data.color  # as hex string
                self.blocks.append(
                    self.Block(
                        self.query_start + arg.runtime_data.start,
                        self.query_start + arg.runtime_data.end,
                        QtGui.QColor(color),
                    )
                )
        event_bus.publish(HighlightingChangedEvent())


class Highlighter(QSyntaxHighlighter):
    def __init__(self, state: HighlightingState, parent=None):
        super().__init__(parent)
        self.logger = logging.getLogger(__name__)
        self.state = state
        event_bus.subscribe(HighlightingChangedEvent, self.on_highlighting_changed)

    def on_highlighting_changed(self, _: HighlightingChangedEvent):
        self.rehighlight()

    def highlightBlock(self, text: str):
        for block in self.state.blocks:
            self.setFormat(block.start, block.end, block.color)


class TextEdit(QtWidgets.QTextEdit):
    key_event = QtCore.Signal(QKeyEvent)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.logger = logging.getLogger(__name__)

    def insert_completion(self, completion):
        tc = self.textCursor()
        extra = len(completion) - len(self.completer.completionPrefix())
        tc.movePosition(QtGui.QTextCursor.MoveOperation.Left)
        tc.movePosition(QtGui.QTextCursor.MoveOperation.EndOfWord)
        tc.insertText(completion[-extra:] + " ")
        self.setTextCursor(tc)

    def textUnderCursor(self):
        tc = self.textCursor()
        tc.select(QtGui.QTextCursor.SelectionType.WordUnderCursor)
        return tc.selectedText()

    def keyPressEvent(self, event: QKeyEvent) -> None:
        self.key_event.emit(event)
        # ignore enter, tab
        if event.key() == Qt.Key_Tab:
            return True
        elif event.key() in (
            Qt.Key_Enter,
            Qt.Key_Return,
            Qt.Key_Tab,
            Qt.Key_Down,
            Qt.Key_Up,
        ):
            self.enter_pressed.emit()
        elif event.key() == Qt.Key_Down:
            self.down_pressed.emit()
        elif event.key() == Qt.Key_Up:
            self.up_pressed.emit()
        else:
            super().keyPressEvent(event)
