# JMenu

Application Launcher for Linux Desktop Environments

## Features

- Extensions
- Ordering
  - Items can be ordered
    - By name
    - Last used
    - Most used
    - Custom
- In App Gui
  - When selecting an item, a gui can be shown to enter input
- Event Driven
- Theming
- Fuzzy Search
