# Authentication

Many extensions need some kind of authentication to the actual service.
Different Services need different authentication methods.

## Requirements

- One extension must not access the authentication data of another extension.
- The authentication data must be stored in a keyring.

## Authentication data

- basic auth
  - username
  - password
- token
  - token
- oauth
  - ...
